package com.androidtutz.anushka.tmdbclient.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;

import com.androidtutz.anushka.tmdbclient.model.Movie;
import com.androidtutz.anushka.tmdbclient.model.MovieDataSource;
import com.androidtutz.anushka.tmdbclient.model.MovieRepository;
import com.androidtutz.anushka.tmdbclient.service.MovieDataService;
import com.androidtutz.anushka.tmdbclient.service.RetrofitInstance;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import kotlinx.coroutines.CoroutineScope;

public class MainActivityViewModel extends AndroidViewModel {
    private MovieRepository movieRepository;

    LiveData<MovieDataSource> movieDataSourceLiveData;
    private Executor executor;
    private LiveData<PagingData<Movie>> moviesPagedList;


    public MainActivityViewModel(@NonNull Application application) {
        super(application);

// CoroutineScope helper provided by the lifecycle-viewmodel-ktx artifact.
        CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(MainActivityViewModel.this);
        MovieDataService movieDataService = RetrofitInstance.getService();

        Executor executor = Executors.newFixedThreadPool(5);
        Pager<Integer, Movie> pager = new Pager<>(
                new PagingConfig(/* pageSize = */ 1),
                () -> new MovieDataSource(movieDataService, application, executor));
        moviesPagedList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager), viewModelScope);

    }

    public LiveData<List<Movie>> getAllMovies() {

        return movieRepository.getMutableLiveData();
    }

    public LiveData<PagingData<Movie>> getMoviesPagedList() {
        return moviesPagedList;
    }
}
