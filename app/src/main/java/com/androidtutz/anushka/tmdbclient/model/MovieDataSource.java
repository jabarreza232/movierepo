package com.androidtutz.anushka.tmdbclient.model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import androidx.paging.PagingState;

import com.androidtutz.anushka.tmdbclient.R;
import com.androidtutz.anushka.tmdbclient.service.MovieDataService;
import com.google.common.util.concurrent.AbstractFuture;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.concurrent.Executor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

public class MovieDataSource extends ListenableFuturePagingSource<Integer, Movie> {
    private MovieDataService movieDataService;
    private Application application;
    @NonNull
    private Executor mBgExecutor;

    public MovieDataSource(MovieDataService movieDataService, Application application, @NonNull Executor bgExecutor) {
        this.movieDataService = movieDataService;
        this.application = application;
        this.mBgExecutor = bgExecutor;
    }


    @NotNull
    @Override
    public ListenableFuture<LoadResult<Integer, Movie>> loadFuture(@NotNull LoadParams<Integer> params) {

        // Start refresh at page 1 if undefined.
        Integer nextPageNumber = params.getKey();
        if (nextPageNumber == null) {
            nextPageNumber = 1;
        }

        ListenableFuture<LoadResult<Integer, Movie>> pageFuture =
                Futures.transform(adapt(movieDataService.getPopularMoviesWithPaging(application.getString(R.string.api_key), nextPageNumber)),
                        MovieDataSource.this::toLoadResult, mBgExecutor);


        ListenableFuture<LoadResult<Integer, Movie>> partialLoadResultFuture =
                Futures.catching(pageFuture, HttpException.class,
                        LoadResult.Error::new, mBgExecutor);

        return Futures.catching(partialLoadResultFuture,
                IOException.class, LoadResult.Error::new, mBgExecutor);
    }

    public ListenableFuture<Response<MovieDBResponse>> adapt(final Call<MovieDBResponse> call) {
        return new AbstractFuture<Response<MovieDBResponse>>() {
            {
                call.enqueue(new Callback<MovieDBResponse>() {
                    @Override
                    public void onResponse(Call<MovieDBResponse> call, Response<MovieDBResponse> response) {
                        set(response);
                    }

                    @Override
                    public void onFailure(Call<MovieDBResponse> call, Throwable t) {
                        setException(t);
                    }
                });
            }

            @Override
            protected void interruptTask() {
                call.cancel();
            }
        };
    }


    @Nullable
    @Override
    public Integer getRefreshKey(@NotNull PagingState<Integer, Movie> state) {
        // Try to find the page key of the closest page to anchorPosition, from
        // either the prevKey or the nextKey, but you need to handle nullability
        // here:
        //  * prevKey == null -> anchorPage is the first page.
        //  * nextKey == null -> anchorPage is the last page.
        //  * both prevKey and nextKey null -> anchorPage is the initial page, so
        //    just return null.
        Integer anchorPosition = state.getAnchorPosition();
        if (anchorPosition == null) {
            return null;
        }

        LoadResult.Page<Integer, Movie> anchorPage = state.closestPageToPosition(anchorPosition);
        if (anchorPage == null) {
            return null;
        }

        Integer prevKey = anchorPage.getPrevKey();
        if (prevKey != null) {
            return prevKey + 1;
        }

        Integer nextKey = anchorPage.getNextKey();
        if (nextKey != null) {
            return nextKey - 1;
        }

        return null;
    }


    public LoadResult<Integer, Movie> toLoadResult(@NonNull Response<MovieDBResponse> response) {
        Log.e("cek:", response.body().getMovies().size() + "");
        return new LoadResult.Page<>(response.body().getMovies(),
                null, // Only paging forward.
                response.body().getPage() + 1,
                LoadResult.Page.COUNT_UNDEFINED,
                LoadResult.Page.COUNT_UNDEFINED);
    }
}
